## pyFileDB ##
###### A simple file system based document database using Redis for indexing ######

####Introduction####
pyFileDB works as follows.

* Takes in a file object
* Saves it within a specific folder
* Indexes the names (for indexing redis is used)
* Returns an id
* Takes in an ID
* Retrieves and returns the file object
* It also takes in 
	* a dictionary to store metadata on each file
	* a list of tags describing the file
* Performs a tag based search

** Yup, its quite simple! **

#### How To ####
First create a redis store
~~~python
import redis
r = redis.StrictRedis(host="localhost", port=6379, db=0)
r.flushall() #for testing
~~~

Then get hold of a file to save
~~~python
file_object = open("testfile.txt","rb")
~~~




Instantiate the pyfileDB class
~~~python
import pyfileDB
db = pyfileDB.fileDB(r, base_folder = None) #base_folder is where the data is stored. Default - pwd
db.initRedis()
~~~

Adding the file_object to the store
~~~python 
id, loc, db_size = db.addFile(f,meta_dict = None, tag_list=["interesting"])
#meta_dict can be any dict describing the file
~~~
Thats it....
* id - id of the file
* loc - the actual location of file
* b_size - number of entreis in store


To retrieve the file
~~~python
return_file_object = db.retrieveFile(id)
~~~
You should get the file object.

To search based on tags...
~~~python
result = db.searchTags['interesting']
~~~
You should get a comma seperated list of file ids.







