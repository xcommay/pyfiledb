"""
fileDB

a filesystem based file storage db designed for archiving
"""
import redis
import os
import hashlib
import sys



class fileDB():
    """
    pyFileDB
    
    A simple document database to store files using the native file system.
    The script stores given files in a series of folders and uses redis to index the file names.
    
    
    """
    
    def __init__(self, r, base_folder = None, files_per_folder = 10):
        """
        
        Parameters
        ----------
        base_folder: the location of the database
        files_per_folder: number of files in each sub folder to bypass fs restrictions.
        
        """
        
        try:
            r.set("test_value_00001000", 0)
            self.r = r
        except:
            print "Can't access reddis"
            sys.exit(1)
        
        if base_folder == None:
            self.base_folder = os.getcwd()
        else:
            self.base_folder = base_folder
            
        self.files_per_folder = 10
        
            

    def initRedis(self):
        """
        WARNING:
        Run this ONLY after creating a new redis instance. This will add system variables into the redis store.
        
        """
        self.r.set('system_current_folder', self.base_folder + "/1")
        self.r.set('system_file_count_in_folder',0)
        self.r.set('system_file_count_total',0)
    
    
    def _getMd5(self,content):
        """
        [NOT USED]
        Using Sha224 instead
        calculates the md5 hash of the content
        """
        m = hashlib.md5()
        m.update(content)
        return m.hexdigest()
        
    def _getSha224(self,content):
        """
        Used to hash the contents
        """
        m = hashlib.sha224()
        m.update(content)
        return m.hexdigest()
        
        
    def _saveFile(self,content, file_name):
        """
        Does the actual saving of the file together with updating the redis store.
        """
        r = self.r
        current_folder = r.get('system_current_folder')
        current_folder_id = int(current_folder.strip().split('/')[-1])
        current_folder_count = int(r.get('system_file_count_in_folder'))
        total_count = int(r.get('system_file_count_total'))
        
        
        if current_folder_count > self.files_per_folder:
            #then create a new folder and add the files there
            new_folder = self.base_folder + "/"+ str(current_folder_id + 1)
            os.makedirs(new_folder)
            #creating pointers to the new folder
            r.set('system_current_folder', new_folder)
            current_folder_count = 0
            r.set('system_file_count_in_folder',current_folder_count)
            
        #now save the file
        current_folder = r.get('system_current_folder')
        #print current_folder, "cf"
        new_file_location = current_folder + '/' + file_name
        #print new_file_location
        ff = open(new_file_location,"wb")
        ff.write(content)
        ff.close()
        
        #updating file details
        r.set(file_name, new_file_location)
        r.set('system_file_count_in_folder', current_folder_count + 1 )
        r.set('system_file_count_total',total_count + 1)
        
        return new_file_location, total_count + 1
    
    def addFile(self,file_object,original_file_name = None,meta_dict = {}, tag_list = []):
        """
        Adds file to the redis store.
        
        Parameters
        ----------
        file_object: file object for the file to be stored. (this uses the file object insead of the file so that the use can also directly sent text through stringio module)
        meta_dict: a dictionary specific to the file to hold meta data
        tag_list: list of tags describing the contents of the file.
        
        Returns
        -------
        
        file_name: the primary key / id and file name of the file. This is also the hash.
        new_file_location: where the file is stored
        total_count: files in the db
        
        
        """
        r = self.r
        content = file_object.read()
        md5 = self._getSha224(content)
        #the md5 will be the filename
        #saveFile(file_object, file_name)
        file_name = md5
        if r.exists(file_name):
            #the file already exists
            print "file_already_exists, adding a link" #[TODO]
            new_file_location = None
            
            total_count = None
            
            
        else:
            new_file_location, total_count = self._saveFile(content, file_name)
            #saving original_file_name
            r.set(file_name+"__original_file_name__", original_file_name)
            #saving meta_dict
            r.set(file_name + "__meta__",meta_dict)
            #adding the tag list
            
            for tag in tag_list:
                t = "__tag__" + str(tag)
                if r.exists(t):
                    #tag exists
                    r.set(t,r.get(t) + "," + file_name)
                else:
                    r.set(t,file_name)
            
            
        
        return file_name, new_file_location, total_count
        
    def getFileLocation(self,file_name):
        r = self.r
        file_location = r.get(file_name)
        return file_location

    def retrieveFile(self, file_name, get_dict = False):
        """
        takes in a file_name and returns the file
        """
        r = self.r
        if not r.exists(file_name):
            meta = None
            print "404"
            return None
        else:
            file_location = r.get(file_name)
            f = open(file_location, "rb")
            
            if get_dict:
                #get the dictionary as well
                meta = r.get(file_name + "__meta__")
            else:
                meta = None
            return f, meta
    
    def deleteFile(self,file_name):
        """
        delete a file form db
        """
        r = self.r
        if r.exists(file_name):
            r.delete(file_name)
            print "deleted " , file_name
        if r.exists(file_name + "__meta__"):
            r.delete(file_name + "__meta__")
        #deal with the tags!
        
    def searchTags(self,tag):
        """
        Returns a document id list for the given tag.
        Currently only seach for one tag.
        """
        
        s = "__tag__" + str(tag).strip()
        
        if r.exists(s):
            return r.get(s)
        else:
            return None
            

